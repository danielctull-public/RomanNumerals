import XCTest

import RomanNumeralsTests

var tests = [XCTestCaseEntry]()
tests += RomanNumeralsTests.__allTests()

XCTMain(tests)
